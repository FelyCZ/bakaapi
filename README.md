# BakaAPI


> Unofficial Bakaláři API library for Android devices. 
---
State: Under early development (not to use in production)
--

DOWNLOAD .AAR (release) HERE: [BakaAPI-lib.aar](https://gitlab.com/FelyCZ/bakaapi/raw/master/bakaapi/build/outputs/aar/bakaapi-release.aar)

DOWNLOAD .APK (for testing, release, unsigned) HERE: [BakaAPI-TestingApp.apk](https://gitlab.com/FelyCZ/bakaapi/raw/master/testingapp/build/outputs/apk/release/testingapp-release-unsigned.apk)

* Programming language: Kotlin
* Minimal SDK version: 19
* Target SDK version: 28

