package cz.fely.libs.bakalari.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.StringRes
import cz.fely.libs.bakalari.R
import kotlinx.android.synthetic.main.loading_overlay.view.*
import java.util.*

class LoadingOverlay(private val ctx: Activity) {
    private var dialog: Dialog = Dialog(ctx)
    var isShown = false
    private val contentView: View = LayoutInflater.from(ctx).inflate(R.layout.loading_overlay, null, false)
    private var message: String = "Loading"

    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(contentView)
        dialog.setCancelable(false)
    }

    fun setMessage(string: String): LoadingOverlay {
        message = string
        contentView.tv_loading_overlay.text = string
        return this
    }

    fun setMessage(@StringRes stringRes: Int): LoadingOverlay {
        if (isShown) {
            timer.cancel()
            timer.purge()
        }
        setMessage(ctx.getString(stringRes))
        if (isShown) {
            timer = Timer("loading_timer")
            timer.scheduleAtFixedRate(PeriodicTask(), 300, 300)
        }
        return this
    }

    fun show(): LoadingOverlay {
        if (!isShown) {
            timer = Timer("loading_timer")
            ctx.window.setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            dialog.show()
            isShown = true

            timer.scheduleAtFixedRate(PeriodicTask(), 300, 300)
        }
        return this
    }

    fun hide(): LoadingOverlay {
        if (isShown) {
            timer.cancel()
            timer.purge()

            ctx.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            dialog.dismiss()
            isShown = false
        }
        return this
    }

    private var dotsCount = 0
    private var timer = Timer("loading_timer")

    private inner class PeriodicTask : TimerTask() {
        @SuppressLint("SetTextI18n")
        override fun run() {
            /* replace with the actual task */
            try {
                if (isShown) {
                    ctx.runOnUiThread {
                        dotsCount++
                        if (dotsCount == 4)
                            dotsCount = 1
                        var dots = ""
                        repeat(dotsCount) {
                            dots += "."
                        }
                        contentView.tv_loading_overlay.text = "$message$dots"
                    }
                }
            } catch (e: Exception) {
                Log.e("LoadingOverlay",e.message)
            }

            /* end task processing */
        }
    }

}