@file:Suppress("UNUSED_PARAMETER", "NOTHING_TO_INLINE")

package cz.fely.libs.bakalari.utils.inline

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar


inline fun View.snackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar = Snackbar
        .make(this, message, Snackbar.LENGTH_SHORT)
        .apply {
            show()
        }
inline fun View?.snackbar(message: CharSequence): Snackbar? = if(this != null) Snackbar
        .make(this, message, Snackbar.LENGTH_SHORT)
        .apply {
            show()
        } else null

inline fun Fragment.snackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar = this.view!!.snackbar(message, actionText, action)
inline fun Fragment.snackbar(message: CharSequence): Snackbar? = this.view?.snackbar(message)

inline fun View?.longSnackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar? = if (this != null) Snackbar
        .make(this, message, Snackbar.LENGTH_LONG)
        .apply {
            show()
        } else null
inline fun View?.longSnackbar(message: CharSequence): Snackbar? = if(this != null) Snackbar
        .make(this, message, Snackbar.LENGTH_LONG)
        .apply {
            show()
        } else null

inline fun Fragment.longSnackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar? = this.view?.longSnackbar(message, actionText, action)
inline fun Fragment.longSnackbar(message: CharSequence): Snackbar? = this.view?.longSnackbar(message)

inline fun View.indefiniteSnackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar = Snackbar
        .make(this, message, Snackbar.LENGTH_INDEFINITE)
        .apply {
            show()
        }

inline fun Fragment.indefiniteSnackbar(message: CharSequence, actionText: CharSequence, noinline action: (View) -> Unit): Snackbar = this.view!!.indefiniteSnackbar(message, actionText, action)