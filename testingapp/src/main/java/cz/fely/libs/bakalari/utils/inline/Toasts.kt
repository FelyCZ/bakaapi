@file:Suppress("NOTHING_TO_INLINE")
package cz.fely.weightedaverage.utils.coroutines

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

inline fun Context.toast(message: CharSequence): Toast = Toast
        .makeText(this, message, Toast.LENGTH_SHORT)
        .apply {
            show()
        }

inline fun Fragment.toast(message: CharSequence): Toast = this.requireActivity().toast(message)

inline fun Context.longToast(message: CharSequence): Toast = Toast
        .makeText(this, message, Toast.LENGTH_LONG)
        .apply {
            show()
        }

inline fun Fragment.longToast(message: CharSequence): Toast = this.requireActivity().longToast(message)