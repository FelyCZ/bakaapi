package cz.fely.libs.bakalari

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import cz.fely.lib.bakalari.api.*
import cz.fely.lib.bakalari.errors.BakaException
import cz.fely.lib.bakalari.models.BakaUser
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.login.Login
import cz.fely.lib.bakalari.models.baka.marks.Subject
import cz.fely.lib.bakalari.models.baka.municipalities.Municipality
import cz.fely.lib.bakalari.models.baka.municipalities.MunicipalityInfo
import cz.fely.lib.bakalari.models.baka.subjects.SubjectInfo
import cz.fely.lib.bakalari.models.baka.tasks.Task
import cz.fely.lib.bakalari.models.baka.teaching.TeachingLesson
import cz.fely.lib.bakalari.models.baka.timetable.Timetable
import cz.fely.libs.bakalari.utils.LoadingOverlay
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var user: BakaUser
    private val prefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    companion object {
        const val PREF_URL = "url"
        const val PREF_NAME = "realName"
        const val PREF_PASS = "pass"
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etUrl.setText(prefs.getString(PREF_URL, ""))
        etName.setText(prefs.getString(PREF_NAME, ""))
        etPass.setText(prefs.getString(PREF_PASS, ""))

        btnLogin.isEnabled = false
        btnGetMarks.isEnabled = false
        btnGetTimetable.isEnabled = false
        btnGetSubjects.isEnabled = false
        btnGetTeaching.isEnabled = false
        btnGetMuni.isEnabled = false

        val loading = LoadingOverlay(this)


        var municipalitiesInfo = emptyList<MunicipalityInfo>()
        btnGetMuniInfo.setOnClickListener {
            loading.show()
            BakaAPI().getMunicipalities(
                { munisInfo: List<MunicipalityInfo> ->
                    tvResult.text = munisInfo.toString()
                    municipalitiesInfo = munisInfo
                    btnGetMuni.isEnabled = true
                }, { ex: BakaException ->
                    tvResult.text = "Error: ${ex.message}"
                }, {
                    loading.hide()
                }
            )
        }
        btnGetMuni.setOnClickListener {
            AlertDialog.Builder(this).apply {
                val spinner = Spinner(this@MainActivity)
                spinner.adapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_dropdown_item,
                    municipalitiesInfo.map { "${it.name} (${it.schoolCount})" })
                setView(spinner)
                setPositiveButton("OK") { dialog, which ->
                    dialog.dismiss()
                    loading.show()
                    BakaAPI().getSchools(municipalitiesInfo[spinner.selectedItemPosition],
                        { municipality: Municipality ->
                            tvResult.text = municipality.toString()
                        }, { ex: BakaException ->
                            tvResult.text = "Error: ${ex.message}"
                        }, {
                            loading.hide()
                        }
                    )
                }
                setNegativeButton("CANCEL") { dialog, which ->
                    dialog.dismiss()
                }
            }.show()
        }
        btnGetHx.setOnClickListener {
            loading.show()
            user = BakaUser(etUrl.text.toString(), etName.text.toString(), etPass.text.toString(),
                Hx(
                    tvType.text.toString(),
                    tvICode.text.toString(),
                    tvSalt.text.toString()
                )
            )

            prefs.edit {
                putString(PREF_URL, user.url)
                putString(PREF_NAME, user.username)
                putString(PREF_PASS, user.password)
            }


            user.fetchHx({ hx: Hx ->
                user = user.setHx(hx)
                tvResult.text = user.toString()
                tvType.text = "Type: " + user.hx!!.type
                tvICode.text = "iCode: " + user.hx!!.iCode
                tvSalt.text = "Salt: " + user.hx!!.salt
                etToken.setText(user.getToken())
                etToken2.setText(user.getTokenHash())
                btnLogin.isEnabled = true
                btnGetMarks.isEnabled = true
                btnGetTimetable.isEnabled = true
                btnGetSubjects.isEnabled = true
            }, { ex: BakaException ->
                tvResult.text = "Error: ${ex.message}"
            },{
                loading.hide()
            })
        }
        btnLogin.setOnClickListener {
            loading.show()
            user.login({ loginInfo: Login ->
                tvResult.text = "$loginInfo\n${loginInfo.modulesList.joinToString(", ")}"
            }, { ex: BakaException ->
                tvResult.text = "Error: ${ex.message}"
            }, {
                loading.hide()
            })
        }
        btnGetMarks.setOnClickListener {
            loading.show()
            user.getMarks({ subjects: List<Subject> ->
                tvResult.text = "${subjects.joinToString(",\n")}\n\n\n${subjects.map { "${it.name}: ${it.marks}\n" }}"
            }, { ex: BakaException ->
                tvResult.text = "Error: ${ex.message}"
            }, {
                loading.hide()
            })
        }
        btnGetTimetable.setOnClickListener {
            AlertDialog.Builder(this).apply {
                val editText = EditText(this@MainActivity)
                editText.hint = "Weeks difference"
                setView(editText)
                setPositiveButton("OK") { dialog, which ->
                    dialog.dismiss()
                    loading.show()
                    user.getTimetable(
                        { tt: Timetable ->
                            tvResult.text = tt.toString()
                        }, { ex: BakaException ->
                            tvResult.text = "Error: ${ex.message}"
                        }, {
                            loading.hide()
                        }, weeksDiff = editText.text.toString().toIntOrNull() ?: 0
                    )

                }
                setNegativeButton("CANCEL") { dialog, which ->
                    dialog.dismiss()
                }
            }.show()
        }
        var subjectsInfo: List<SubjectInfo> = emptyList()
        btnGetSubjects.setOnClickListener {
            loading.show()
            user.getSubjects(
                { subsInfo: List<SubjectInfo> ->
                    tvResult.text = subsInfo.toString()
                    subjectsInfo = subsInfo
                    btnGetTeaching.isEnabled = true
                }, { ex: BakaException ->
                    tvResult.text = "Error: ${ex.message}"
                }, {
                    loading.hide()
                }
            )
        }
        btnGetTeaching.setOnClickListener {
            AlertDialog.Builder(this).apply {
                val spinner = Spinner(this@MainActivity)
                spinner.adapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_spinner_dropdown_item,
                    subjectsInfo.map { "${it.subjectCode} - ${it.name}" }.sorted())
                setView(spinner)
                setPositiveButton("OK") { dialog, which ->
                    dialog.dismiss()
                    loading.show()
                    user.getTeaching(
                        (subjectsInfo.sortedBy { it.subjectCode })[spinner.selectedItemPosition].subjectCode,
                        { teachingLessons: List<TeachingLesson> ->
                            tvResult.text = teachingLessons.sortedBy { it.order }.toString()
                        }, { ex: BakaException ->
                            tvResult.text = "Error: ${ex.message}"
                        }, {
                            loading.hide()
                        }
                    )

                }
                setNegativeButton("CANCEL") { dialog, which ->
                    dialog.dismiss()
                }
            }.show()
        }
        btnGetTasks.setOnClickListener {
            loading.show()
            user.getTasks(
                { tasks: List<Task> ->
                    tvResult.text = "${tasks.toString()}\n\n${tasks.map { it.taskAttachments.toString() + "\n" }.toString()}"
                }, { ex: BakaException ->
                    tvResult.text = "Error: ${ex.message}"
                }, {
                    loading.hide()
                }
            )
        }
    }
}