package cz.fely.lib.bakalari.models.baka.tasks

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class Task(

    @SerializedName("predmet")
    val subject: String,

    @SerializedName("zkratka")
    val shortcut: String,

    @SerializedName("zadano")
    val assignedStr: String,

    @SerializedName("nakdy")
    val toWhenStr: String,

    @SerializedName("popis")
    val description: String,

    @SerializedName("status")
    val status: String,

    @SerializedName("typ")
    val type: String,

    @SerializedName("id")
    val id: String,

    @SerializedName("attachments")
    val attachmentsObj_: JsonElement

): IGsonModel, GsonModelSerializable {
    private val attachmentsObj: TaskAttachments?
        get() = if (attachmentsObj_ is JsonObject) Gson().fromJson(attachmentsObj_)
                else null

    val taskAttachments: List<TaskAttachment>
        get() = attachmentsObj?.attachments ?: emptyList()

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Task> {
        override fun fromJson(json: String): Task {
            return Gson().fromJson(json)
        }
    }
}