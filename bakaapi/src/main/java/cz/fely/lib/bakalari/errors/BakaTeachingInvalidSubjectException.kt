package cz.fely.lib.bakalari.errors

class BakaTeachingInvalidSubjectException(
    override val code: Int,
    override val message: String,
    override val stackTrace: String?,
    subjectCode: String
) : BakaException(code, message, stackTrace)