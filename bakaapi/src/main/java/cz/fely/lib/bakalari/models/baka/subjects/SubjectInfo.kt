package cz.fely.lib.bakalari.models.baka.subjects

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class SubjectInfo(

    @SerializedName("nazev")
    val name: String,

    @SerializedName("zkratka")
    val shortcut: String,

    @SerializedName("kod_pred")
    val subjectCode: String,

    @SerializedName("ucitel")
    val teacher: String,

    @SerializedName("zkratkauc")
    val teacherShortcut: String,

    @SerializedName("mailuc")
    val teacherEmail: String,

    @SerializedName("telefonuc")
    val teacherPhone: String
): IGsonModel, GsonModelSerializable {

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<SubjectInfo> {
        override fun fromJson(json: String): SubjectInfo {
            return Gson().fromJson(json)
        }
    }
}