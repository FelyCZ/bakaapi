package cz.fely.lib.bakalari.models.baka.login

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class Login(
    @SerializedName("verze")
    val version: String,

    @SerializedName("jmeno")
    val name: String,

    @SerializedName("typ")
    val type: String,

    @SerializedName("strtyp")
    val typeStr: String,

    @SerializedName("skola")
    val school: String,

    @SerializedName("typskoly")
    val schoolType: String,

    @SerializedName("trida")
    val clazz: String,

    @SerializedName("rocnik")
    val grade: Int,

    @SerializedName("moduly")
    val modules: String

    //, @SerializedName("params") val params: List<String> //TODO: Fix problem with parsing
): IGsonModel, GsonModelSerializable {
    val modulesList: List<String>
        get() = modules.substring(IntRange(1, modules.length - 2)).split("*")

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Login> {
        override fun fromJson(json: String): Login {
            return Gson().fromJson(json)
        }
    }
}