package cz.fely.lib.bakalari.models.baka.tasks

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class TaskAttachments(
    @SerializedName("attachment")
    val attachments_: JsonElement
): IGsonModel, GsonModelSerializable {
    val attachments: List<TaskAttachment>
        get() = if (attachments_ is JsonObject) listOf(Gson().fromJson(attachments_))
                else Gson().fromJson(attachments_)
    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<TaskAttachments> {
        override fun fromJson(json: String): TaskAttachments {
            return Gson().fromJson(json)
        }
    }
}