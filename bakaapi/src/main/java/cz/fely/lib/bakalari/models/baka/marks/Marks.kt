package cz.fely.lib.bakalari.models.baka.marks

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.marks.Mark
import cz.fely.lib.bakalari.util.inline.fromJson

data class Marks(

    @SerializedName("znamka")
    val marks: List<Mark>

): IGsonModel, GsonModelSerializable {
    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Marks> {
        override fun fromJson(json: String): Marks {
            return Gson().fromJson(json)
        }
    }
}