package cz.fely.lib.bakalari.models.baka.schools

import com.google.gson.*
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.schools.SchoolInfo
import cz.fely.lib.bakalari.util.inline.fromJson

data class Schools(
    @SerializedName("schoolInfo")
    val schoolsObj: JsonElement
): IGsonModel, GsonModelSerializable {

    val schools: List<SchoolInfo>
        get() = when (schoolsObj) {
            is JsonObject -> listOf(Gson().fromJson(schoolsObj))
            is JsonArray -> Gson().fromJson(schoolsObj)
            else -> emptyList()
        }

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Schools> {
        override fun fromJson(json: String): Schools {
            return Gson().fromJson(json)
        }
    }
}