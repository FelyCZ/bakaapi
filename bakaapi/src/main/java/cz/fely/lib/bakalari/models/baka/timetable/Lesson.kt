package cz.fely.lib.bakalari.models.baka.timetable

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson
import java.text.SimpleDateFormat
import java.util.*

data class Lesson(
    @SerializedName("caption")
    val caption: String,

    @SerializedName("begintime")
    val beginTimeStr: String,

    @SerializedName("endtime")
    val endTimeStr: String
): IGsonModel, GsonModelSerializable {
    val beginTime: Date
        get() = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
            .parse(beginTimeStr)
    val endTime: Date
        get() = SimpleDateFormat(TIME_FORMAT, Locale.getDefault())
            .parse(endTimeStr)

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Lesson> {
        private const val TIME_FORMAT = "HH:mm"

        override fun fromJson(json: String): Lesson {
            return Gson().fromJson(json)
        }
    }
}