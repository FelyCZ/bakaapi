package cz.fely.lib.bakalari.models.baka.hx

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.util.inline.fromJson

data class Hx(
    @SerializedName("typ")
    val type: String,

    @SerializedName("ikod")
    val iCode: String,

    @SerializedName("salt")
    val salt: String
): IGsonModel, GsonModelSerializable {
    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Hx> {
        override fun fromJson(json: String): Hx {
            return Gson().fromJson(json)
        }
    }
}