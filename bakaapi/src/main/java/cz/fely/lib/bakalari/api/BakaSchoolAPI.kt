@file:Suppress("KDocUnresolvedReference")

package cz.fely.lib.bakalari.api

import android.util.Log
import com.github.kittinunf.fuel.core.isServerError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import cz.fely.lib.bakalari.BAKA_DATE_FORMAT
import cz.fely.lib.bakalari.errors.*
import cz.fely.lib.bakalari.models.BakaUser
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.login.Login
import cz.fely.lib.bakalari.models.baka.marks.Subject
import cz.fely.lib.bakalari.models.baka.subjects.SubjectInfo
import cz.fely.lib.bakalari.models.baka.tasks.Task
import cz.fely.lib.bakalari.models.baka.teaching.TeachingLesson
import cz.fely.lib.bakalari.models.baka.timetable.Timetable
import cz.fely.lib.bakalari.util.inline.fromJson
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


private object ParamNames {
    const val GET_HX = "getHx"
    const val HX = "hx"
    const val PM = "pm"
    const val PMD = "pmd"
}

private object ModuleNames {
    const val LOGIN = "login"
    const val MARKS = "znamky"
    const val TIMETABLE = "rozvrh"
    const val SUBJECTS = "predmety"
    const val TEACHING = "vyuka"
    const val TASKS = "ukoly"
}

/**
 * @param BakaUser is without Hx (hx == null)
 * @param onSuccess(Hx): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.fetchHx(
    onSuccess: (hx: Hx) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    this.url
        .httpGet(listOf(ParamNames.GET_HX to this.username))
        .responseString { req, response, result ->
            Log.d("BApi - fetchHx", req.url.toString())
            when (result) {
                is Result.Failure -> {
                    val error = result.error
                    onFailure(
                        BakaRequestException(
                            BakaErrors.Codes.WEB_COMMUNICATION,
                            "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                            response.isServerError,
                            error.stackTrace?.contentToString()
                        )
                    )
                }
                is Result.Success -> {
                    val xml = result.get()
                    val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")

                    val res = json?.getInt("res")
                    if (res == null || res == 2) {
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_REQUEST,
                                BakaErrors.Messages.WEB_REQUEST
                            )
                        )
                    } else {
                        val hx = Gson().fromJson<Hx>(json.toString())
                        onSuccess(hx as Hx)
                    }
                }
            }
        }
    finally?.invoke()
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(Login): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.login(
    onSuccess: (login: Login) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        this.url
            .httpGet(listOf(ParamNames.HX to token, ParamNames.PM to ModuleNames.LOGIN))
            .responseString { req, response, result ->
                Log.d("BApi - login", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")

                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {
                            val login = Gson().fromJson<Login>(json.toString())
                            onSuccess(login as Login)
                        }
                    }
                }
            }
    }
    finally?.invoke()
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(List<Subjects>): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.getMarks(
    onSuccess: (subjects: List<Subject>) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        this.url
            .httpGet(listOf(ParamNames.HX to token, ParamNames.PM to ModuleNames.MARKS))
            .responseString { req, response, result ->
                Log.d("BApi - getMarks", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")
                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {
                            val subjectJsonTree = json.opt("predmety")
                            val subjects = if (subjectJsonTree is JSONObject) {
                                val subjectsArr = subjectJsonTree.opt("predmet")
                                when (subjectsArr) {
                                    is JSONArray -> Gson().fromJson(subjectsArr.toString())
                                    is JSONObject -> Gson().fromJson(subjectsArr.toString())
                                    else -> emptyList()
                                }
                            } else emptyList<Subject>()

                            onSuccess(subjects)
                        }
                    }
                }
            }
        finally?.invoke()
    }
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(List<Subjects>): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally() action to do, when method finishes
 * @param weeksDiff difference in required week and this week (should be positive or negative, default: 0 - means this week)
 * @return nothing, handle it via actions
 */
fun BakaUser.getTimetable(
    onSuccess: (timetable: Timetable) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null,
    weeksDiff: Int = 0
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        val cal = Calendar.getInstance()
        cal.add(Calendar.WEEK_OF_YEAR, weeksDiff)

        while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
            cal.add(Calendar.DAY_OF_WEEK, -1)

        val dateStr = SimpleDateFormat(BAKA_DATE_FORMAT, Locale.getDefault()).format(cal.time)

        this.url
            .httpGet(
                listOf(
                    ParamNames.HX to token,
                    ParamNames.PM to ModuleNames.TIMETABLE,
                    ParamNames.PMD to dateStr
                )
            )
            .responseString { req, response, result ->
                Log.d("BApi - getTimetable", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")
                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {
                            val timetable =
                                Gson().fromJson<Timetable>(json.getJSONObject("rozvrh").toString())
                            onSuccess(timetable)
                        }
                    }
                }
            }
    }
    finally?.invoke()
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(List<SubjectInfo>): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.getSubjects(
    onSuccess: (subjects: List<SubjectInfo>) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        this.url
            .httpGet(listOf(ParamNames.HX to token, ParamNames.PM to ModuleNames.SUBJECTS))
            .responseString { req, response, result ->
                Log.d("BApi - getSubjects", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")

                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {

                            val subs = json.getJSONObject("predmety").opt("predmet")
                            val subjects = if (subs is JSONArray) {
                                Gson().fromJson<List<SubjectInfo>>(subs.toString())
                            } else {
                                emptyList()
                            }
                            onSuccess(subjects)
                        }
                    }
                }
            }
    }
    finally?.invoke()
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(List<SubjectInfo>): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.getTeaching(
    subjectCode: String,
    onSuccess: (teachingLessons: List<TeachingLesson>) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        this.url
            .httpGet(
                listOf(
                    ParamNames.HX to token,
                    ParamNames.PM to ModuleNames.TEACHING,
                    ParamNames.PMD to subjectCode
                )
            )
            .responseString { req, response, result ->
                Log.d("BApi - getTeaching", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")

                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {
                            val teaching = json.opt("vyuka")
                            if (teaching is JSONObject) {
                                if (!teaching.has("vyhodina"))
                                    onFailure(
                                        BakaTeachingInvalidSubjectException(
                                            BakaErrors.Codes.TEACHING_INVALID_SUBJECT,
                                            BakaErrors.Messages.TEACHING_INVALID_SUBJECT,
                                            null,
                                            subjectCode
                                        )
                                    )
                                else {
                                    val teachingLessons =
                                        Gson().fromJson<List<TeachingLesson>>(
                                            teaching.getJSONArray(
                                                "vyhodina"
                                            ).toString()
                                        )
                                    onSuccess(teachingLessons)
                                }
                            } else {
                                onSuccess(emptyList())
                            }
                        }
                    }
                }
            }
    }
    finally?.invoke()
}

/**
 * @param BakaUser is WITH Hx (hx != null), else (hx == null) it will fail
 * @param onSuccess(List<SubjectInfo>): action to do, when it finishes successfully
 * @param onFailure(BakaException): action do do, when it failed (successfully)
 * @param finally(): action to do, when method finishes
 * @return nothing, handle it via actions
 */
fun BakaUser.getTasks(
    onSuccess: (tasks: List<Task>) -> Unit,
    onFailure: (ex: BakaException) -> Unit,
    finally: (() -> Unit)? = null
) {
    val token = this.getTokenHash()
    if (token == null) {
        onFailure(BakaTokenException(BakaErrors.Codes.NULL_TOKEN, BakaErrors.Messages.NULL_TOKEN))
    } else {
        this.url
            .httpGet(listOf(ParamNames.HX to token, ParamNames.PM to ModuleNames.TASKS))
            .responseString { req, response, result ->
                Log.d("BApi - getTasks", req.url.toString())
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {


                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("results")

                        val res = json?.getInt("result")
                        if (res == null || res == -1) {
                            onFailure(
                                BakaRequestException(
                                    BakaErrors.Codes.LOGIN_ERROR,
                                    BakaErrors.Messages.LOGIN_ERROR
                                )
                            )
                        } else {
                            val tasksObj = json.opt("ukoly")

                            val tasksList: List<Task>

                            tasksList = if (tasksObj is JSONObject) {
                                when (val tasks = tasksObj.opt("ukol")) {
                                    is JSONArray -> Gson().fromJson(tasks.toString())
                                    is JSONObject -> listOf(Gson().fromJson(tasks.toString()))
                                    else -> emptyList()
                                }
                            } else {
                                emptyList()
                            }
                            onSuccess(tasksList)

                        }
                    }
                }
            }
    }
    finally?.invoke()
}