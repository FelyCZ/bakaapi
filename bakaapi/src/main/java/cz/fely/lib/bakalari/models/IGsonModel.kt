package cz.fely.lib.bakalari.models


interface IGsonModel {
}

interface GsonModelSerializable {
    fun toJson(): String
}

interface GsonModelDeserializer<IGsonModel: cz.fely.lib.bakalari.models.IGsonModel> {
    fun fromJson(json: String): IGsonModel
}