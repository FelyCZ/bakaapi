package cz.fely.lib.bakalari.models.baka.teaching

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.BAKA_DATE_FORMAT
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson
import java.text.SimpleDateFormat
import java.util.*

data class TeachingLesson (
    @SerializedName("poradi")
    val order: Int,

    @SerializedName("datum")
    val dateStr: String,

    @SerializedName("hodina")
    val lesson: Int,

    @SerializedName("cislo_hod")
    val lessonNum: Int,

    @SerializedName("tema")
    val topic: String,

    @SerializedName("poznamka")
    val note: String
): IGsonModel, GsonModelSerializable {

    val date: Date
        get() = SimpleDateFormat(BAKA_DATE_FORMAT, Locale.getDefault()).parse(dateStr)

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }
    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<TeachingLesson> {
        override fun fromJson(json: String): TeachingLesson {
            return Gson().fromJson(json)
        }
    }
}