package cz.fely.lib.bakalari.models

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.BakaToken
import cz.fely.lib.bakalari.util.inline.fromJson

class BakaUser(@SerializedName("school_address")
               val url: String,

               @SerializedName("username")
               val username: String,

               @SerializedName("plain_password")
               val password: String,

               @SerializedName("hx")
               val hx: Hx? = null
): IGsonModel, GsonModelSerializable {
    fun getToken(): String? = if (hx == null) null else BakaToken.get(username, password, hx)
    fun getTokenHash(): String? = if(hx == null) null else BakaToken.getHash(username, password, hx)

    fun setHx(hx: Hx) = BakaUser(url, username, password, hx)

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<BakaUser> {
        override fun fromJson(json: String): BakaUser {
            return Gson().fromJson(json)
        }
    }
}