package cz.fely.lib.bakalari.models.baka.municipalities

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.schools.SchoolInfo
import cz.fely.lib.bakalari.models.baka.schools.Schools
import cz.fely.lib.bakalari.util.inline.fromJson

data class Municipality(
    @SerializedName("name")
    val searchedName: String,

    @SerializedName("schools")
    val schoolsObj: Schools
): IGsonModel, GsonModelSerializable {

    val schools: List<SchoolInfo>
        get() = schoolsObj.schools

    var realName: String = searchedName


    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Municipality> {
        override fun fromJson(json: String): Municipality {
            return Gson().fromJson(json)
        }
    }
}