package cz.fely.lib.bakalari.errors

class BakaRequestException(override val code: Int,
                           override val message: String,
                           val isServerError: Boolean? = null,
                           override val stackTrace: String? = null)
    : BakaException(code, message, stackTrace) {

    override fun getLog(): String {
        return "@$date || Code: $code || Message: $message || IsServerError: $isServerError || StackTrace: $stackTrace"
    }
}