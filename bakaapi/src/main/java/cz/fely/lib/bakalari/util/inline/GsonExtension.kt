package cz.fely.lib.bakalari.util.inline

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken

inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)
inline fun <reified T> Gson.fromJson(json: JsonElement) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)