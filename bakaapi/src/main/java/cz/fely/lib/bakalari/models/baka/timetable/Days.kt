package cz.fely.lib.bakalari.models.baka.timetable

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class Days(
    @SerializedName("pocet")
    val count: Int,

    @SerializedName("den")
    val days: List<Day>
): IGsonModel, GsonModelSerializable {

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Days> {
        override fun fromJson(json: String): Days {
            return Gson().fromJson(json)
        }
    }
}