package cz.fely.lib.bakalari.errors

class BakaErrors {
    object Codes {
        const val NULL_TOKEN = 1
        const val WEB_REQUEST = 2
        const val WEB_COMMUNICATION = 3
        const val INVALID_JSON_FORMAT = 4
        const val LOGIN_ERROR = 5
        const val TEACHING_INVALID_SUBJECT = 6
    }
    object Messages {
        const val NULL_TOKEN = "token is null"
        const val WEB_REQUEST = "web request failed"
        const val WEB_COMMUNICATION = "web communication failed"
        const val INVALID_JSON_FORMAT = "invalid JSON format"
        const val LOGIN_ERROR = "login error / invalid password"
        const val TEACHING_INVALID_SUBJECT = "subjectCode was not found in module teaching"
    }
}