package cz.fely.lib.bakalari.util

import cz.fely.lib.bakalari.BAKA_DATE_FORMAT
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.crypto.Base64
import cz.fely.lib.bakalari.util.crypto.SHA512
import java.text.SimpleDateFormat
import java.util.*


class BakaToken() {
    companion object {
        fun get(username: String, password: String, hx: Hx): String {
            return "*login*$username*pwd*${Base64.encode(SHA512.hashFrom("${hx.salt}${hx.iCode}${hx.type}$password"))}*sgn*ANDR"
        }
        fun getHash(username: String, password: String, hx: Hx): String {
            val dateFormatted = SimpleDateFormat(BAKA_DATE_FORMAT, Locale.getDefault()).format(Date())
            val hash = Base64.encode(SHA512.hashFrom(
               "${get(username, password, hx)}$dateFormatted"))
            return hash.replace("\\", "_").replace("/", "_").replace("+", "-")
        }
    }
}