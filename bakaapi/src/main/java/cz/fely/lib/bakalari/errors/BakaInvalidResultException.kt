package cz.fely.lib.bakalari.errors

class BakaInvalidResultException(override val code: Int, override val message: String,
                                 override val stackTrace: String? = null): BakaException(code, message, stackTrace)