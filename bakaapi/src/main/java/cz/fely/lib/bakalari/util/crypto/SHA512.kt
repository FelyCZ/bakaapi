package cz.fely.lib.bakalari.util.crypto

import java.security.MessageDigest

class SHA512 {
    companion object {
        fun hashFrom(text: String): ByteArray {
            val d = MessageDigest.getInstance("SHA-512")
            return d.digest(text.toByteArray())
        }
    }
}