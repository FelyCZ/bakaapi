package cz.fely.lib.bakalari.models.baka.timetable

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson

data class DayLesson(
    @SerializedName("idcode")
    val idCode: String,

    @SerializedName("typ")
    val type: String,

    @SerializedName("nazev")
    val name: String,

    @SerializedName("zkrpr")
    val subjectShortcut: String,

    @SerializedName("pr")
    val subject: String,

    @SerializedName("zkruc")
    val teacherShortcut: String,

    @SerializedName("uc")
    val teacher: String,

    @SerializedName("zkrmist")
    val roomShortcut: String,

    @SerializedName("mist")
    val room: String,

    @SerializedName("zkrabs")
    val absenceShortcut: String,

    @SerializedName("abs")
    val absence: String,

    @SerializedName("tema")
    val topic: String,

    @SerializedName("zkrskup")
    val groupShortcut: String,

    @SerializedName("skup")
    val group: String,

    @SerializedName("cycle")
    val cycle: String,

    @SerializedName("uvol")
    val release: String,

    @SerializedName("chng")
    val change: String,

    @SerializedName("caption")
    val caption: String,

    @SerializedName("notice")
    val notice: String,

    @SerializedName("zkratka")
    val shortcut: String
): IGsonModel, GsonModelSerializable {


    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<DayLesson> {
        override fun fromJson(json: String): DayLesson {
            return Gson().fromJson(json)
        }
    }
}