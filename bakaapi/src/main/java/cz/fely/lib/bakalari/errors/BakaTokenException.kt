package cz.fely.lib.bakalari.errors

import java.lang.Exception

class BakaTokenException(override val code: Int, override val message: String) : BakaException(code, message) {

}