package cz.fely.lib.bakalari.models.baka.timetable

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.BakaUser
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.util.inline.fromJson

data class Timetable(
    @SerializedName("typ")
    val type: String,

    @SerializedName("kodcyklu")
    val cycleCode: Int,

    @SerializedName("zkratkacyklu")
    val cycleShortcut: String,

    @SerializedName("nazevcyklu")
    val cycleName: String,

    @SerializedName("hodiny")
    val lessonsInfo: Lessons,

    @SerializedName("dny")
    val daysInfo: Days
): IGsonModel, GsonModelSerializable {

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Timetable> {
        override fun fromJson(json: String): Timetable {
            return Gson().fromJson(json)
        }
    }
}