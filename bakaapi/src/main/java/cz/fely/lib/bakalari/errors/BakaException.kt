package cz.fely.lib.bakalari.errors

import java.lang.Exception
import java.util.*

open class BakaException(open val code: Int, override val message: String, open val stackTrace: String? = null, val date: Date = Date()) : Exception(message) {
    open fun getLog(): String {
        return "@$date || Code: $code || Message: $message || StackTrace: $stackTrace"
    }
}