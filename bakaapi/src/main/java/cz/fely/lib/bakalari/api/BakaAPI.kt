package cz.fely.lib.bakalari.api

import android.net.Uri
import android.util.Log
import com.github.kittinunf.fuel.core.isServerError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import cz.fely.lib.bakalari.errors.BakaErrors
import cz.fely.lib.bakalari.errors.BakaException
import cz.fely.lib.bakalari.errors.BakaRequestException
import cz.fely.lib.bakalari.models.baka.municipalities.Municipality
import cz.fely.lib.bakalari.models.baka.municipalities.MunicipalityInfo
import cz.fely.lib.bakalari.util.inline.fromJson
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import java.net.URL

private const val BAKA_URL = "https://sluzby.bakalari.cz/api/v1"

class BakaAPI {
    fun getMunicipalities(
        onSuccess: (municipalitiesInfo: List<MunicipalityInfo>) -> Unit,
        onFailure: (ex: BakaException) -> Unit,
        finally: (() -> Unit)? = null
    ) {
        "$BAKA_URL/municipality"
            .httpGet()
            .responseString { _, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("ArrayOfmunicipalityInfo")
                            ?.getJSONArray("municipalityInfo")

                        val municipalitiesInfo =
                            Gson().fromJson<List<MunicipalityInfo>>(json.toString()).filter { it.name.isNotEmpty() }

                        onSuccess(municipalitiesInfo)
                    }
                }
                finally?.invoke()
            }
    }

    fun getSchools(
        municipalityInfo: MunicipalityInfo,
        onSuccess: (municipality: Municipality) -> Unit,
        onFailure: (ex: BakaException) -> Unit,
        finally: (() -> Unit)? = null
    ) {
        var cityEnc = Uri.encode(municipalityInfo.name)
        if (cityEnc.contains("."))
            cityEnc = cityEnc.substring(IntRange(0, cityEnc.indexOf(".") - 1))

        val url = URL("$BAKA_URL/municipality/$cityEnc")
        val str = url.toExternalForm()
        str
            .httpGet()
            .responseString { _, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val error = result.error
                        onFailure(
                            BakaRequestException(
                                BakaErrors.Codes.WEB_COMMUNICATION,
                                "${BakaErrors.Messages.WEB_COMMUNICATION}: ${error.message}",
                                response.isServerError,
                                error.stackTrace?.contentToString()
                            )
                        )
                    }
                    is Result.Success -> {
                        val xml = result.get()
                        val json = XmlToJson.Builder(xml).build().toJson()?.getJSONObject("municipality")

                        val municipality =
                            Gson().fromJson<Municipality>(json.toString())
                        onSuccess(municipality.apply {
                            this.realName = municipalityInfo.name
                        })
                    }
                }
                finally?.invoke()
            }
    }
}
