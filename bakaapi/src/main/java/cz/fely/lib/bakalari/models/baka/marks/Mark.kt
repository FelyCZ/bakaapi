package cz.fely.lib.bakalari.models.baka.marks

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.util.inline.fromJson
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

data class Mark(
    @SerializedName("pred")
    val sub: String,

    @SerializedName("maxb")
    val maxPts: Int,

    @SerializedName("znamka")
    val markStr: String,

    @SerializedName("zn")
    val markStr2: String,

    @SerializedName("bd")
    val pts: String,

    @SerializedName("datum")
    val dateStr: String,

    @SerializedName("udeleno")
    val grantedStr: String,

    @SerializedName("vaha")
    val weight: Int,

    @SerializedName("caption")
    val caption: String,

    @SerializedName("poznamka")
    val note: String,

    @SerializedName("typ")
    val type: String,

    @SerializedName("ozn")
    val ozn: String
): IGsonModel, GsonModelSerializable {

    val mark: Double
        get() {
            var markStrTemp = markStr
            if (listOf("A", "N", "!").contains(markStrTemp)) {
                Log.e("Mark", "mark contains invalid character(s)")
                return 0.0
            }

            var markTemp = 0.0
            if (markStrTemp.endsWith("-")) {
                markTemp += 0.5
                markStrTemp = markStrTemp.substring(IntRange(0, markStrTemp.length - 2))
            }
            markTemp += try {
                markStrTemp.toDouble()
            } catch (ex: Exception) {
                Log.e("Mark", "could not parse mark - ${ex.message}")
                0.0
            }
            return markTemp
        }
    val mark2: Double
        get() {
            var markStrTemp = markStr2
            if (listOf("A", "N", "!").contains(markStrTemp)) {
                Log.e("Mark", "mark contains invalid character(s)")
                return 0.0
            }

            var markTemp = 0.0
            if (markStrTemp.endsWith("-")) {
                markTemp += 0.5
                markStrTemp = markStrTemp.substring(IntRange(0, markStrTemp.length - 2))
            }
            markTemp += try {
                markStrTemp.toDouble()
            } catch (ex: Exception) {
                Log.e("Mark", "could not parse mark - ${ex.message}")
                0.0
            }
            return markTemp
        }
    val date: Date
        get() {
            return try {
                SimpleDateFormat("yyMMdd", Locale.getDefault()).parse(dateStr)
            } catch (ex: Exception) {
                Log.e("Mark", "could not parse date - ${ex.message}")
                Date()
            }
        }
    val granted: Date
        get() {
            return try {
                SimpleDateFormat("yyMMddHHmm", Locale.getDefault()).parse(grantedStr)
            } catch (ex: Exception) {
                Log.e("Mark", "could not parse date - ${ex.message}")
                Date()
            }
        }

    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object: GsonModelDeserializer<Mark> {
        override fun fromJson(json: String): Mark {
            return Gson().fromJson(json)
        }
    }
}