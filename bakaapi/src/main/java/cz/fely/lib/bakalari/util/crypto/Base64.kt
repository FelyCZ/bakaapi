package cz.fely.lib.bakalari.util.crypto

import android.util.Base64

class Base64 {
    companion object {
        fun encode(text: ByteArray): String {
            return Base64.encodeToString(text, Base64.NO_WRAP)
        }
        fun encode(text: String): String {
            return encode(text.toByteArray())
        }

        fun decode(base64: String): String {
            return String(Base64.decode(base64, Base64.NO_WRAP))
        }
    }
}