package cz.fely.lib.bakalari.models.baka.marks

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.annotations.*
import cz.fely.lib.bakalari.models.GsonModelDeserializer
import cz.fely.lib.bakalari.models.GsonModelSerializable
import cz.fely.lib.bakalari.models.IGsonModel
import cz.fely.lib.bakalari.models.baka.hx.Hx
import cz.fely.lib.bakalari.models.baka.marks.Mark
import cz.fely.lib.bakalari.models.baka.marks.Marks
import cz.fely.lib.bakalari.util.inline.fromJson

data class Subject(
    @SerializedName("nazev")
    val name: String,

    @SerializedName("zkratka")
    val shortcut: String,

    @SerializedName("prumer")
    val average: String,

    @SerializedName("numprumer")
    val numAvg: String,

    @SerializedName("prepocet")
    val recalculation: String,

    @SerializedName("bodytoznm")
    val pts2Mark: String,

    @SerializedName("ctvrt")
    val ctvrt: String,

    @SerializedName("pozn")
    val note: String,

    @SerializedName("globpozn")
    val globalNote: String,

    @SerializedName("znamky")
    val marksObj: JsonElement
) : IGsonModel, GsonModelSerializable {
    val marks: List<Mark>
        get() = if (marksObj is JsonPrimitive) emptyList()
        else Gson().fromJson<Marks>(marksObj).marks


    override fun toString(): String {
        return GsonBuilder().setPrettyPrinting().create().toJson(this)
    }

    override fun toJson(): String {
        return Gson().toJson(this)
    }

    companion object : GsonModelDeserializer<Subject> {
        override fun fromJson(json: String): Subject {
            return Gson().fromJson(json)
        }
    }
}